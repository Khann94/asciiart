package Window.Builder;

import javax.swing.*;
import java.io.FileWriter;
import java.io.PrintWriter;

/**
 * Created by Karol on 2016-05-22.
 */
public class SaveFile {
    public JFileChooser fileChooserhooser = new JFileChooser();

    public void save(char[][] ascii){
        int result = fileChooserhooser.showSaveDialog(null);
                if(result == JFileChooser.APPROVE_OPTION){
                    try{
                        PrintWriter writer = new PrintWriter(fileChooserhooser.getSelectedFile()+".txt");
                        for(int i=0; i< ascii.length; i++){
                            for(int j = 0; j< ascii[i].length; j++){
                                writer.print(ascii[i][j]);
                            }
                            writer.println();
                        }
                        writer.close();
                    }catch (Exception ex){
                        ex.printStackTrace();
                    }
                }
    }
}

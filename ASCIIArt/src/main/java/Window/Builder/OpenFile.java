package Window.Builder;
import javax.swing.*;
import java.util.Scanner;
import java.util.Scanner.*;
/**
 * Created by Karol on 2016-05-19.
 */
public class OpenFile {
    public JFileChooser fileChooserhooser = new JFileChooser();
    public StringBuilder sb = new StringBuilder();

    public void Open() throws Exception{
        if(fileChooserhooser.showOpenDialog(null) == JFileChooser.APPROVE_OPTION){
            java.io.File file = fileChooserhooser.getSelectedFile();
            Scanner input = new Scanner(file);

                sb.append(file.getAbsolutePath());

            input.close();
        }
        else{
            sb.append("No file was selected");
        }
    }

}

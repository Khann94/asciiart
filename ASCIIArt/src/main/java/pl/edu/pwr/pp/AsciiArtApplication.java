package pl.edu.pwr.pp;

import javafx.embed.swing.JFXPanel;

import javax.swing.*;
import java.net.URISyntaxException;
import java.io.IOException;

public class AsciiArtApplication {

	public static void main(String[] args) {
		JFrame frame = new JFrame("Form");
		Form forma = new Form();
		forma.Wyswietl();

		String[] images = new String[]{"Marilyn_Monroe", "Mona_Lisa", "Sierpinski_Triangle"};
		String pgmExtension = ".pgm";
		String txtExtension = ".txt";
		
		ImageFileReader imageFileReader = new ImageFileReader(); 
		ImageFileWriter imageFileWriter = new ImageFileWriter();
		
		for (String imageName : images) {
			try {
				// przeczytaj plik pgm
				int[][] intensities = imageFileReader.readPgmFile(imageName + pgmExtension);
				// przekształć odcienie szarości do znaków ASCII
				char[][] ascii = ImageConverter.intensitiesToAscii(intensities);
				// zapisz ASCII art do pliku tekstowego
				imageFileWriter.saveToTxtFile(ascii, imageName + txtExtension);
			} catch (IOException e){
				e.printStackTrace();
			} catch (URISyntaxException e) {
				e.printStackTrace();
			} 
		}
	}
}

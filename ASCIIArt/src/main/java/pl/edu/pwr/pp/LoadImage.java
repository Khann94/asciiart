package pl.edu.pwr.pp;

import Window.Builder.OpenFile;

import javax.swing.*;
import javax.swing.filechooser.FileNameExtensionFilter;
import java.awt.event.*;

import static com.sun.deploy.uitoolkit.ToolkitStore.dispose;

/**
 * Created by Karol on 2016-05-20.
 */
public class LoadImage extends JDialog {
    public JPanel LoadImagePanel;
    private JButton OKButton;
    private JButton anulujButton;
    private JRadioButton URLRadioButton;
    private JRadioButton zDyskuRadioButton;
    private JButton wybierzPlikButton;
    private JTextField DiscField;
    private JTextField URLField;
    private String pathToFile;

    public String getPath(){
        return pathToFile;
    }

    public boolean urlSelected() {

        return URLRadioButton.isSelected();
    }
    public boolean localSelected() {

        return zDyskuRadioButton.isSelected();
    }

    public String PathReturn(){
        setBounds(0,0,300,250);
        setVisible(true);
        String path = "";
        if(zDyskuRadioButton.isSelected()) {
            pathToFile = DiscField.getText().toString();
            return DiscField.getText();
        }
        if(URLRadioButton.isSelected())
            return URLField.getText();
        else
            return path;
    }

    public LoadImage() {
        setContentPane(LoadImagePanel);
        setModal(true);
        getRootPane().setDefaultButton(OKButton);

        OKButton.setEnabled(false);
        wybierzPlikButton.setEnabled(false);
        DiscField.setEnabled(false);
        URLField.setEnabled(false);

        wybierzPlikButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OpenFile of = new OpenFile();
                try{
                    of.Open();
                }catch (Exception e1){
                    e1.printStackTrace();
                }
                DiscField.setText(of.sb.toString());
            }
        });

        zDyskuRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OKButton.setEnabled(zDyskuRadioButton.isSelected() || URLRadioButton.isSelected());

                if(URLRadioButton.isSelected()){
                    URLRadioButton.setSelected(false);
                    URLRadioButton.setEnabled(false);
                    URLField.setEnabled(false);
                }
                URLRadioButton.setEnabled(true);
                wybierzPlikButton.setEnabled(zDyskuRadioButton.isSelected());
                DiscField.setEnabled(zDyskuRadioButton.isSelected());
            }
        });

        URLRadioButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                OKButton.setEnabled(zDyskuRadioButton.isSelected() || URLRadioButton.isSelected());

                if(zDyskuRadioButton.isSelected()){
                    zDyskuRadioButton.setSelected(false);
                    zDyskuRadioButton.setEnabled(false);
                    wybierzPlikButton.setEnabled(false);
                    DiscField.setEnabled(false);
                }
                zDyskuRadioButton.setEnabled(true);
                URLField.setEnabled(URLRadioButton.isSelected());
            }
        });
        anulujButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            dispose();
            }
        });
        OKButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                dispose();
            }
        });
    }
}


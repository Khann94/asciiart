package pl.edu.pwr.pp;

import Window.Builder.SaveFile;

import javax.imageio.ImageIO;
import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.image.WritableRaster;
import java.io.File;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.net.URL;


/**
 * Created by Karol on 2016-05-19.
 */
public class Form {
    private JButton funkcja2Button;
    private JButton WczytajObrazButton;
    private JButton funkcja1Button;
    private JButton zapiszDoPlikuButton;
    private JButton Opcja2Button;
    private JButton Opcja1Button;
    private JPanel PanelButton;
    private JPanel ImagePanel;
    private JLabel ImageLabel;
    private JPanel DialogPanel;
    LoadImage loadImage = new LoadImage();



    public void Wyswietl(){
        JFrame frame = new JFrame("Form");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        JPanel panel = new JPanel();
        panel.add(PanelButton);
        panel.add(ImagePanel);
       // panel.add(new JLabel(new ImageIcon("C:/Users/Karol/Desktop/AsciiArt/ASCIIArt/src/main/resources/The_Witcher_Confrontation.jpg")));
        frame.add(panel);
        frame.pack();
        frame.setVisible(true);
        zapiszDoPlikuButton.setEnabled(false);
    }

    public Form() {
        WczytajObrazButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
            ShowImage();
            }
        });
        zapiszDoPlikuButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                saveImage(loadImage.getPath());
            }
        });
    }

    private void ShowImage(){
        String path = loadImage.PathReturn();
        System.out.println(path);
        if(loadImage.localSelected())
            showLocalImage(path);
        if(loadImage.urlSelected())
            ShowURLImage(path);
    }

    private void showLocalImage(String path){
            BufferedImage image = null;
        try
        {
        File file = new File(path);
        if (path.contains("pgm")) {
            ImageFileReader reader = new ImageFileReader();
            Path p = Paths.get(path);
            String fileName = p.getFileName().toString();

            int rawImage[][] = reader.readPgmFile(fileName);
            int imageWidth = rawImage[0].length;
            int imageHeight = rawImage.length;

            image = new BufferedImage(imageWidth, imageHeight,
                    BufferedImage.TYPE_BYTE_GRAY);
            WritableRaster raster = image.getRaster();
            for (int y = 0; y < imageHeight; y++) {
                for (int x = 0; (x < imageWidth); x++) {
                    raster.setSample(x, y, 0, rawImage[y][x]);
                }
            }
            image.setData(raster);
            zapiszDoPlikuButton.setEnabled(true);
        } else {
            image = ImageIO.read(file);
            zapiszDoPlikuButton.setEnabled(false);
        }
    }
    catch (Exception e)
    {
        e.printStackTrace();
        System.exit(1);
    }
        ImageIcon newImage = new ImageIcon(image);
        ImageLabel.setIcon(newImage);
        ImagePanel.setSize(ImageLabel.getSize());
    }

    private void saveImage(String path){

        File file = new File(path);
        try {
            ImageFileReader reader = new ImageFileReader();
            SaveFile saveFile = new SaveFile();
            Path p = Paths.get(path);
            String fileName = p.getFileName().toString();

            int[][] intensities = reader.readPgmFile(fileName);
//         przekształć odcienie szarości do znaków ASCII
            char[][] ascii = ImageConverter.intensitiesToAscii(intensities);
            // zapisz ASCII art do pliku tekstowego
            saveFile.save(ascii);
        }
        catch (Exception e){
            e.printStackTrace();
            System.exit(1);
        }
    }

    public void ShowURLImage(String path){
        BufferedImage image = null;
        try {
            URL url = new URL(path);
            //File file = url.getFile();
            InputStream i = null;
            try{
                i = url.openStream();
            }catch (UnknownHostException ex) {
                ex.printStackTrace();
                JOptionPane.showMessageDialog(null,"Nieprawidlowy adres URL");
            }
            if (i != null) {
                image = ImageIO.read(url);
                if(image == null) {
                    JOptionPane.showMessageDialog(null,"Adres jest prawidlowy lecz nie mozna poprac obrazu. Format URL powinien byc \"https:// ... .(rozszerzenie obrazu)\"");
                }
            }
            else{
                JOptionPane.showMessageDialog(null,"Nieprawidlowy adres URL");
            }
        } catch (Exception e) {
            e.printStackTrace();
            System.exit(1);
        }
        ImageIcon imageIcon = new ImageIcon(image);
        ImageLabel.setIcon(imageIcon);
        ImagePanel.setSize(ImageLabel.getSize());
    }
}

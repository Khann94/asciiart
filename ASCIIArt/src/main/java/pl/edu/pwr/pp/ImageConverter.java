package pl.edu.pwr.pp;

public class ImageConverter {

	/**
	 * Znaki odpowiadajÄ…ce kolejnym poziomom odcieni szaroĹ›ci - od czarnego (0)
	 * do biaĹ‚ego (255).
	 */
	public static String INTENSITY_2_ASCII = "@%#*+=-:. ";

	/**
	 * Metoda zwraca znak odpowiadajÄ…cy danemu odcieniowi szaroĹ›ci. Odcienie
	 * szaroĹ›ci mogÄ… przyjmowaÄ‡ wartoĹ›ci z zakresu [0,255]. Zakres jest dzielony
	 * na rĂłwne przedziaĹ‚y, liczba przedziaĹ‚Ăłw jest rĂłwna iloĹ›ci znakĂłw w
	 * {@value #INTENSITY_2_ASCII}. Zwracany znak jest znakiem dla przedziaĹ‚u,
	 * do ktĂłrego naleĹĽy zadany odcieĹ„ szaroĹ›ci.
	 * 
	 * 
	 * @param intensity
	 *            odcieĹ„ szaroĹ›ci w zakresie od 0 do 255
	 * @return znak odpowiadajÄ…cy zadanemu odcieniowi szaroĹ›ci
	 */
	public static char intensityToAscii(int intensity) {

		if (intensity < 26) {
			return '@';
		}
		if ((26 <= intensity) && (intensity < 52)) {
			return '%';
		}
		if ((52 <= intensity) && (intensity < 77)) {
			return '#';
		}
		if ((77 <= intensity) && (intensity < 103)) {
			return '*';
		}
		if ((103 <= intensity) && (intensity < 128)) {
			return '+';
		}
		if ((128 <= intensity) && (intensity < 154)) {
			return '=';
		}
		if ((154 <= intensity) && (intensity < 180)) {
			return '-';
		}
		if ((180 <= intensity) && (intensity < 205)) {
			return ':';
		}
		if ((205 <= intensity) && (intensity < 231)) {
			return '.';
		}
		if ((231 <= intensity) && (intensity <= 255)) {
			return ' ';
		} else
			// jesli z poza zakresu to pusty znak
			return ' ';
	}

	/**
	 * Metoda zwraca dwuwymiarowÄ… tablicÄ™ znakĂłw ASCII majÄ…c dwuwymiarowÄ…
	 * tablicÄ™ odcieni szaroĹ›ci. Metoda iteruje po elementach tablicy odcieni
	 * szaroĹ›ci i dla kaĹĽdego elementu wywoĹ‚uje {@ref #intensityToAscii(int)}.
	 * 
	 * @param intensities
	 *            tablica odcieni szaroĹ›ci obrazu
	 * @return tablica znakĂłw ASCII
	 */
	public static char[][] intensitiesToAscii(int[][] intensities) {
		int row = intensities.length;
		int col = intensities[0].length;
		char[][] tabascii = new char[row][col];
		for (int i = 0; i < row; i++) {
			for (int j = 0; j < intensities[i].length; j++) {
				tabascii[i][j] = intensityToAscii(intensities[i][j]);
			}
		}
		return tabascii;
	}

}
